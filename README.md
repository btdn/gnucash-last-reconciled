# gnucash-last-reconciled

## Introduction

gnucash-last-reconciled is an ANSI Common Lisp system for interacting with [GnuCash](https://gnucash.org/) via an sqlite or MySQL database. The intended use case for providing automatic reminders of accounts that are overdue for reconciliation.

## Getting Started

1. Copy `contrib/setup.lisp.example` to `contrib/setup.lisp`.
2. Edit the `setup.lisp`, setting `*accounts*` as desired.
3. Run one of the example scripts:
	* `sbcl --non-interactive --load ./contrib/cron.lisp --eval '(cl-dbi:with-connection (connection :mysql :host "127.0.0.1" :database-name "gnucash") (cron-process *accounts* connection))'`
	* `sbcl --non-interactive --load ./contrib/csv.lisp --eval '(cl-dbi:with-connection (connection :mysql :host "127.0.0.1" :database-name "gnucash") (csv-process *accounts* connection))'`
	* `sbcl --non-interactive --load ./contrib/common.lisp --eval '(cl-dbi:with-connection (connection :mysql :host "127.0.0.1" :database-name "gnucash") (process *accounts* connection))'`

## Example Output

This is an example of output from `csv-process`:

	The following may be overdue for reconciliation:

	Awesome Rewards Credit Card
		Actual: 2018-09-17
    	Expected: 2019-01-18

	Savings
		Actual: 2018-11-30
		Expected: 2019-01-31

	Lisp Machines, Inc.
		 Actual: 2018-06-30
   		 Expected: 2018-12-31

