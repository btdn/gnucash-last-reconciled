(asdf:defsystem "gnucash-last-reconciled"
    :author "Benjamin Newman"
    :depends-on ("sxql"
		 "cl-dbi"
		 "cl-date-time-parser"
		 "local-time")
    :components ((:module "src"
			  :serial t
			  :components ((:file "packages")
				       (:file "guid")
				       (:file "gnucash")
				       (:file "gnucash-last-reconciled"))))
			  :in-order-to ((test-op (test-op "gnucash-last-reconciled/tests"))))

(asdf:defsystem "gnucash-last-reconciled/tests"
    :depends-on (:gnucash-last-reconciled :fiveam :local-time)
    :components ((:module "t"
			  :serial t
			  :components ((:file "packages")
				       (:file "gnucash-last-reconciled"))))
    :perform (test-op (o s)
		      (uiop:symbol-call :gnucash-last-reconciled-tests
					'all-tests)))
