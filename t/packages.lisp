(defpackage gnucash-last-reconciled-tests
  (:use :cl :fiveam)
  (:export :run!
       :all-tests))
