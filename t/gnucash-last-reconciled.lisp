(in-package :gnucash-last-reconciled-tests)

(def-suite gnucash-last-reconciled-tests
    :description "Test middleware layer.")

(in-suite gnucash-last-reconciled-tests)

(defun all-tests ()
  (run! 'gnucash-last-reconciled-tests))

(defparameter *1-to-15*
  '(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15))
(defparameter *1-to-28*
  '(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28))
(defparameter *1-to-30*
  '(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30))
(defparameter *1-to-31*
  '(1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31))
(defparameter *2-to-12*
   '(2 3 4 5 6 7 8 9 10 11 12))
(defparameter *16-to-28*
  '(16 17 18 19 20 21 22 23 24 25 26 27 28))
(defparameter *16-to-30*
  '(16 17 18 19 20 21 22 23 24 25 26 27 28 29 30))
(defparameter *16-to-31*
  '(16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31))

(defconstant +middle-day-of-month+
  15)

(defun encode (day month year)
  (gnucash-last-reconciled::encode day month year))

(defun case-month (month 28-days 30-days 31-days)
  (ecase month
    (2 28-days)
    ((4 6 9 11) 30-days)
    ((1 3 5 7 8 10 12) 31-days)))

(defun days-in-month (month)
  (case-month month 28 30 31))

(defun days-in-month-list (month)
  (case-month month *1-to-28* *1-to-30* *1-to-31*))

(test test-month-to-quarter
  (flet ((test-one (expected month)
	   (is (= expected (gnucash-last-reconciled::month-to-quarter month)))))		  
    (dolist (month '(1 2 3))
      (test-one 1 month))
    (dolist (month '(4 5 6))
      (test-one 2 month))
    (dolist (month '(7 8 9))
      (test-one 3 month))
    (dolist (month '(10 11 12))
      (test-one 4 month))))

(test test-get-last-quarter-date
  (labels ((test-one (expected day month year)
	     (is (local-time:timestamp= expected
				   (gnucash-last-reconciled::get-last-quarter-date
				    (encode day month year)))))
	   (test-month (expected month year)
	     (dolist (day (case-month month *1-to-28* *1-to-30* *1-to-31*))
	       (test-one expected day month year))))
  (let ((year 2019)
	(previous-year 2018))
    (dolist (month '(1 2 3))
      (test-month (encode 31 12 previous-year) month year))
    (dolist (month '(4 5 6))
      (test-month (encode 31 3 year) month year))
    (dolist (month '(7 8 9))
      (test-month (encode 30 6 year) month year))
    (dolist (month '(10 11 12))
      (test-month (encode 30 9 year) month year)))))

(test test-get-last-day-of-previous-month
  (flet ((test-one (expected day month year)
	   (is (local-time:timestamp= expected
				   (gnucash-last-reconciled::get-last-day-of-previous-month
				    (encode day month year))))))
  (let ((year 2019)
	(previous-year 2018))
    (let ((month 1)
	  (expected (encode 31 12 previous-year)))
      (dolist (day (days-in-month-list month))
	(test-one expected day month year)))
    (dolist (month *2-to-12*)
      (let ((expected (encode (days-in-month (1- month)) (1- month) year)))
	(dolist (day (days-in-month-list month))
	  (test-one expected day month year)))))))

(test test-get-last-month-date-middle
  (labels ((test-one (expected day-of-month day month year)
	     (is (local-time:timestamp= expected
					(gnucash-last-reconciled::get-last-month-date
					 day-of-month
					 (encode day month year)))))
	   (test-month (expected-previous-month expected-this-month later-days month year)
	     (dolist (day *1-to-15*)
	       (test-one expected-previous-month +middle-day-of-month+ day month year))
	     (dolist (day later-days)
	       (test-one expected-this-month +middle-day-of-month+ day month year))))
    (let ((year 2019)
	  (previous-year 2018))
      (let ((month 1))
	(test-month (encode +middle-day-of-month+ 12 previous-year)
		    (encode +middle-day-of-month+ month year)
		    *16-to-31*
		    month
		    year))
      (dolist (month *2-to-12*)
	(test-month (encode +middle-day-of-month+ (1- month) year)
		    (encode +middle-day-of-month+ month year)
		    (case-month month *16-to-28* *16-to-30* *16-to-31*)
		    month
		    year)))))

(test test-schedule-to-timestamp-quarterly
      (labels ((test-one (expected day month year)
		 (is (local-time:timestamp= expected
					    (gnucash-last-reconciled::schedule-to-timestamp "quarterly"
											    (encode day month year)))))
	       (test-month (expected month year)
		 (dolist (day (case-month month *1-to-28* *1-to-30* *1-to-31*))
		   (test-one expected day month year))))
	(let ((year 2019)
	      (previous-year 2018))
	  (dolist (month '(1 2 3))
	    (test-month (encode 31 12 previous-year) month year))
	  (dolist (month '(4 5 6))
	    (test-month (encode 31 3 year) month year))
	  (dolist (month '(7 8 9))
	    (test-month (encode 30 6 year) month year))
	  (dolist (month '(10 11 12))
	    (test-month (encode 30 9 year) month year)))))

(test test-schedule-to-timestamp-last-day
  (flet ((test-one (expected day month year)
	   (is (local-time:timestamp= expected
				   (gnucash-last-reconciled::schedule-to-timestamp "monthly:last-day"
				    (encode day month year))))))
  (let ((year 2019)
	(previous-year 2018))
    (let ((month 1)
	  (expected (encode 31 12 previous-year)))
      (dolist (day (days-in-month-list month))
	(test-one expected day month year)))
    (dolist (month *2-to-12*)
      (let ((expected (encode (days-in-month (1- month)) (1- month) year)))
	(dolist (day (days-in-month-list month))
	  (test-one expected day month year)))))))
