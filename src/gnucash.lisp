(in-package :gnucash)

(defparameter *connection*
  nil
  "Default database connection handle.")

(defun sxql (function sxql &optional (connection *connection*))
  (assert connection)
  (multiple-value-bind (sql parameters) (yield sxql)
    (funcall function (apply #'dbi:execute (prepare connection sql) parameters))))

(defun fetch (sxql &optional (connection *connection*))
  (gnucash::sxql #'cl-dbi:fetch sxql connection))

(defun fetch-all (sxql &optional (connection *connection*))
  (gnucash::sxql #'cl-dbi:fetch-all sxql connection))

(defun last-reconciled-by-account-guid (account-guid &optional (connection *connection*))
  (let ((result (fetch (select (:s1.int64_val)
			 (from (:as :slots :s1))
			 (inner-join (:as :slots :s2)
				     :on (:and (:= :s2.name "reconcile-info")
					       (:= :s2.obj_guid account-guid)
					       (:= :s2.guid_val :s1.obj_guid)))
			 (where (:and (:= :s1.name "reconcile-info/last-date")
				      (:= :s1.numeric_val_denom "1"))))
		       connection)))
    (when result
      (unix-to-timestamp (second result)))))

(defun account-name-by-guid (account-guid &optional (connection *connection*))
  (let ((result (fetch (select (:name)
			 (from :accounts)
			 (where (:= :guid account-guid)))
		       connection)))
    (when result
      (second result))))

(defun children-accounts-by-parent-guid (account-guid &optional (connection *connection*))
  (let ((result (fetch-all (select (:guid)
			     (from :accounts)
			     (where (:= :parent_guid account-guid)))
			   connection)))
    (when result
      (mapcar #'second result))))
