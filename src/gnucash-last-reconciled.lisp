(in-package :gnucash-last-reconciled)

(defconstant +months-per-quarter+ 3)
(defparameter +quarterly-schedule-string+ "quarterly")
(defparameter +monthly-schedule-prefix+ "monthly:")
(defparameter +monthly-schedule-prefix-length+ (length +monthly-schedule-prefix+))
(defparameter +monthly-schedule-last-day-suffix+ "last-day")

(defparameter *today* (today))

(defstruct (account (:conc-name acct-))
  (guid nil
	:type (or nil guid)
	:read-only t)
  (schedule ""
	    :type string
	    :read-only t)
  (recurse nil
	   :type boolean))

(defgeneric get-account-tree (account connection))

(defmethod get-account-tree ((account-guid string) connection)
  (gnucash:children-accounts-by-parent-guid account-guid connection))

(defmethod get-account-tree ((account account) connection)
  (get-account-tree (acct-guid account) connection))

(defun encode (day month year)
  (declare (fixnum day month year))
  (encode-timestamp 0 0 0 0 day month year))

(defun month-to-quarter (month)
  (ceiling month +months-per-quarter+))

(defun get-last-quarter-date (&optional (today *today*))
  (let ((year (timestamp-year today))
	(month (timestamp-month today)))
    (ecase (month-to-quarter month)
      (4 (encode 30 9 year))
      (3 (encode 30 6 year))
      (2 (encode 31 3 year))
      (1 (encode 31 12 (1- year))))))

(defun get-last-day-of-previous-month (&optional (today *today*))
  (timestamp- (timestamp-minimize-part today :day) 1 :day))

(defun get-last-month-date (day-of-month &optional (today *today*))
  (declare ((integer 1 28) day-of-month))
  (let* ((year (timestamp-year today))
	 (month (timestamp-month today))
	 (this-month (encode day-of-month month year)))
    (if (timestamp< this-month today)
	this-month
        (timestamp- (encode day-of-month month year)
		1
		:month))))

(defun starts-with-p (str prefix)
  (declare (string str prefix))
  (let ((p (search prefix str)))
    (and p (= 0 p))))

(defun schedule-to-timestamp (schedule &optional (today *today*))
  (declare (string schedule))
  (cond ((string= schedule +quarterly-schedule-string+)
	 (get-last-quarter-date today))
	((starts-with-p schedule +monthly-schedule-prefix+)
	 (let ((day (subseq schedule 8)))
	   (if (equal day +monthly-schedule-last-day-suffix+)
	       (get-last-day-of-previous-month today)
	       (get-last-month-date (parse-integer day)))))))

(defun is-past-due (actual expected)
  (if actual
      (timestamp< actual expected)
      T))

(defun account-is-past-due (account-guid schedule connection)
  (let* ((actual (last-reconciled-by-account-guid account-guid connection))
	 (expected (schedule-to-timestamp schedule))
	 (past-due (is-past-due actual expected)))
    (values past-due
	    actual
	    expected)))
