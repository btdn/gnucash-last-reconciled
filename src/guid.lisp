(in-package :guid)

(deftype guid () '(simple-string 32))
