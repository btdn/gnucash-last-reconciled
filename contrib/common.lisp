(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp"
                                       (user-homedir-pathname))))
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))

(ql:quickload 'gnucash-last-reconciled)

(use-package (list :gnucash :gnucash-last-reconciled))

(load (merge-pathnames "common-lisp/gnucash-last-reconciled/contrib/setup.lisp"
		       (user-homedir-pathname)))

(defun format-date (timestamp)
  (when timestamp
    (local-time:format-rfc3339-timestring nil timestamp :omit-time-part T)))

(defmethod ac-name ((account-guid string) connection)
  (account-name-by-guid account-guid connection))

(defmethod ac-name ((account account) connection)
    (ac-name (acct-guid account) connection))

(defun process (accounts connection
		&key
		  (print-account-p-fn (lambda (is-past-due)
					is-past-due))
		  (print-account-fn (lambda (name actual-timestamp expected-timestamp name-prefix)
				      (format t "~A~A ~A ~A~%~%" name-prefix name actual-timestamp expected-timestamp)))
		  (print-header-p-fn (lambda (printed-header)
				       (not printed-header)))
		  (print-header-fn (lambda ()
				     (format t "The following may be overdue for reconciliation:~%~%")))
		  (recurse-p-fn (lambda (do-recurse)
				  do-recurse))
		  ;; intended for recursion:
		  (printed-header nil)
		  (name-prefix ""))
    (dolist (account accounts)
      (multiple-value-bind (is-past-due actual expected)
	  (account-is-past-due (acct-guid account) (acct-schedule account) connection)
	(when (funcall print-account-p-fn is-past-due)
	  (when (funcall print-header-p-fn printed-header)
	    (setf printed-header t)
	    (funcall print-header-fn))
	  (funcall print-account-fn
		   (ac-name account connection)
		   actual
		   expected
		   name-prefix))
	(when (funcall recurse-p-fn (acct-recurse account))
	  (labels ((apply-schedule (subaccount-guid)
		     (make-account :guid subaccount-guid
				   :schedule (acct-schedule account))))
	    (process (map 'list
			  #'apply-schedule
			  (children-accounts-by-parent-guid (acct-guid account) connection))
		     connection
		     :print-account-p-fn print-account-p-fn
		     :print-account-fn print-account-fn
		     :print-header-p-fn print-header-p-fn
		     :print-header-fn print-header-fn
		     :recurse-p-fn recurse-p-fn
		     :printed-header printed-header
		     :name-prefix (format nil "~a:" (ac-name account connection))))))))
