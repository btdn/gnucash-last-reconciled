(load #P"contrib/common")
(ql:quickload 'cl-csv)

(defun csv-process (accounts connection)
  (labels ((print-account-p-fn (is-past-due)
	     (declare (ignore is-past-due))
	     t)
    (format-is-past-due (is-past-due)
	     (when is-past-due
	       "TRUE"))
	   (print-header-fn ()
	     (cl-csv:write-csv-row (list "account"
					 "actual"
					 "expected"
					 "past-due")
				   :stream *standard-output*))
	   (print-account (name actual expected name-prefix)
	     (cl-csv:write-csv-row (list (concatenate 'string name-prefix name)
					 (format-date actual)
					 (format-date expected)
					 (format-is-past-due (is-past-due actual expected)))
				   :stream *standard-output*)))
    (process accounts
	     connection
	     :print-account-p-fn #'print-account-p-fn
	     :print-header-fn #'print-header-fn
	     :print-account-fn #'print-account)))

;;(sqlite:with-open-database (db *in-path*)
;;  (csv-process db *accounts*))
