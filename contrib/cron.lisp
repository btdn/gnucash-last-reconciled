(load #P"contrib/common")

(defun cron-process (accounts connection)
  (labels ((print-header ()
	     (format t "The following may be overdue for reconciliation:~%~%"))
	   (print-account (name actual-timestamp expected-timestamp name-prefix)
	     (declare (type (or local-time:timestamp null) actual-timestamp)
		      (local-time:timestamp expected-timestamp))
	     (format t
		     "~a~a~%    Actual: ~a~%    Expected: ~a~%~%"
		     name-prefix
		     name
		     (format-date actual-timestamp)
		     (format-date expected-timestamp))))
    (process accounts
	     connection
	     :print-header-fn #'print-header
	     :print-account-fn #'print-account)))

;(cl-dbi:with-connection (connection
;			 :mysql
;			 :host "127.0.0.1"
;			 :database-name "gnucash"
;			 :username "gnucash-last-reconciled"
;			 :password "gnucash-last-reconciled")
;  (cron-process *accounts* connection))
